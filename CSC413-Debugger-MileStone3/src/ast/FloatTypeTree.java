package ast;

import visitor.*;

/**
 *
 * @author Frank Dye
 */
public class FloatTypeTree extends AST
{

    public FloatTypeTree()
    {
    }

    public Object accept(ASTVisitor v)
    {
        return v.visitFloatTypeTree(this);
    }

}

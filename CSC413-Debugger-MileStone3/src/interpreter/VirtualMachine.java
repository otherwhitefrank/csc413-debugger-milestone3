package interpreter;

import interpreter.bytecodes.ByteCode;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Frank Dye
 */
public class VirtualMachine {

    private Program program; //Ref to the program that is executing
    private int pc;          //Current program counter
    private boolean isRunning; //Running a program or not
    protected RunTimeStack runStack; //The runtime stack
    private boolean isDumping;     //Output debug info ?
    private ByteCode currBC;       //The currently executing ByteCode

    private Stack<Integer> returnStack;

    /**
     * Default constructor, sets up VirtualMachine, inits it to the passed in
     * Program
     *
     * @param program
     */
    public VirtualMachine(Program program) {
        //Initialize the VirtualMachine object
        this.program = program;
        pc = 0;
        isRunning = false;
        runStack = new RunTimeStack();
        returnStack = new Stack<Integer>();
        isDumping = false;

    }

    /**
     * Return a List of the args on the top of the stack
     *
     * @return List object containing each argument on the top of the runStack
     */
    public List<String> getArgs() {
        return runStack.getArgs();
    }

    /**
     * Switch Debug Dumping on
     */
    public void dumpOn() {
        isDumping = true;
    }

    /**
     * Switch Debug Dumping off
     */
    public void dumpOff() {
        isDumping = false;
    }

    /**
     * Ask the RunTimeStack to create a new frame at offset
     *
     * @param offset
     */
    public void newFrameAt(int offset) {
        //Pass in offset and the current program counter
        runStack.newFrameAt(offset);
    }

    /**
     * Ask the RunTimeStack to peek at the top of the stack without modifying
     * value
     *
     * @return top of RunTimeStack
     */
    public int peekStack() {
        return runStack.peek();
    }

    /**
     * Ask the RunTimeStack to pop one item off its top
     *
     * @return top of RunTimeStack
     */
    public int popStack() {
        return runStack.pop();
    }

    /**
     * Ask the RunTimeStack to push val ontop of itself
     *
     * @param val
     * @return val pushed on top of RunTimeStack
     */
    public int pushStack(int val) {
        runStack.push(val);
        return runStack.peek();
    }

    /**
     * Read an Integer from the user
     *
     * @return Integer read from the user
     */
    public int readInt() {
        int val = 0;
        Scanner in = new Scanner(System.in); //Open standard input
        while (true)
        {
            try
            {

                System.out.println("? ");
                String stringVal = in.next();
                val = Integer.parseInt(stringVal);
                break; //Successful conversion to int, break loop

            } catch (NumberFormatException ne)
            {
                System.out.println("Input is not a number, please re-enter.");
            }

        }
        return val;
    }

    /**
     * Write top of RunTimeStack to the stdout
     */
    public void writeInt() {
        int val = 0;

        val = runStack.peek();

        System.out.println(val);
    }

    /**
     * Ask the RunTimeStack to pop the top frame on its framePointer
     *
     */
    public void popFrame() {
        //Pop the current frame off the runStack
        runStack.popFrame();
    }

    /**
     * Ask the RunTimeStack to store the top of the stack at offset from current
     * frame
     *
     * @param offset
     * @return val that was stored at offset from current frame
     */
    public int storeStackOffset(int offset) {
        return runStack.store(offset);
    }

    /**
     * Ask the RunTimeStack to load the value at offset from current frame to
     * top of stack
     *
     * @param offset
     * @return val that is loaded to top of RunTimeStack
     */
    public int loadStackOffset(int offset) {
        //Tell stack to retrieve frame + offset and push it on the top of the stack
        return runStack.load(offset);
    }

    /**
     * Set the current execution of the VM to addr
     *
     * @param addr
     */
    public void setProgramCounter(int addr) {
        //Used by GotoCode and similar to set the Program Counter to a new branch address
        isRunning = true;
        pc = addr;
    }

    /**
     * Tell the VM to halt its execution.
     */
    public void haltExecution() {
        //Set is running false
        isRunning = false;
        //System.out.println("HALT\n");
        //VM should wind down
    }

    /**
     * Tell VM to begin executing Program
     */
    public void executeProgram() {
        try
        {
            //Acquire the first line of the program
            currBC = program.requestByteCodeAtAddr(pc);
            isRunning = true; //Begin program execution, HALT must be hit for this to be set false
            while (isRunning)
            {
                //Execute the ByteCode
                pc++; //Increment program counter to our next instruction
                //this is done here so GotoCode, etc, can reset the program counter
                //and the VM will branch naturally
                currBC.execute(this);

                if (isDumping)
                {

                    String debugOutput = currBC.getDebugLine(this);
                    if (debugOutput != null)
                    {
                        System.out.println(debugOutput);

                    }
                    runStack.dump();//Debug
                    System.out.println();

                }

                //Only resolve ByteCode if the vm is still running
                if (isRunning)
                {
                    currBC = program.requestByteCodeAtAddr(pc); //Get next ByteCode
                }
            }

        } catch (ByteCodeSyntaxErrorException ex)
        {
            System.out.println("Error running ByteCode, stopping in executeProgram()\n");
            System.exit(1);
        }

    }

    public int getValAtOffset(int offset) {
        return this.runStack.getValAtOffset(offset);
    }

    /**
     * Used by CallCode to update the return address in the case when we have
     * instructions after the Args command.
     */
    public void pushReturnAddress() {
        //Push the current PC down in the returnStack.
        returnStack.push(pc);
    }

    /**
     *
     */
    public int popReturnAddress() {
        return returnStack.pop();
    }
}

package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * BOP ByteCode, takes one argument
 * @author frank
 */
public class BopCode extends ByteCode {
    
    String operation; //Holds the operation to perform

    public BopCode() {
        super("BopCode");
    }

    /**
     * Retrieves the two top slots on the runStack, then performs specified operation
     * pushing the returnVal on the top of runStack
     * @param vm
     */
    @Override
    public void execute(VirtualMachine vm) {
        int op1, op2;
        op2 = vm.popStack();
        op1 = vm.popStack();
        String oper = this.operation;
        int returnVal = 0;
        switch (oper) {
            case "+":
                returnVal = op1 + op2;
                break;
            case "-":
                returnVal = op1 - op2;
                break;
            case "/":
                returnVal = op1 / op2;
                break;
            case "*":
                returnVal = op1 * op2;
                break;
            case "==":
                returnVal = (op1 == op2) ? 1 : 0;
                break;
            case "!=":
                returnVal = (op1 != op2) ? 1 : 0;
                break;
            case "<=":
                returnVal = (op1 <= op2) ? 1 : 0;
                break;
            case ">":
                returnVal = (op1 > op2) ? 1 : 0;
                break;
            case ">=":
                returnVal = (op1 >= op2) ? 1 : 0;
                break;
            case "<":
                returnVal = (op1 < op2) ? 1 : 0;
                break;
            case "|":
                if (op1 == 0 && op2 == 0) {
                    returnVal = 0;
                } else {
                    returnVal = 1;
                }
                break;
            case "&":
                if (op1 == 1 && op2 == 1) {
                    returnVal = 1;
                } else {
                    returnVal = 0;
                }
                break;
        }
        vm.pushStack(returnVal);
    }

    /**
     * Standard Debug Print
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //BOP simply prints the original sourceByteCode
        return this.getSourceByteCode();    
    }

    /**
     * Standard init for ByteCode
     * @param args
     */
    @Override
    public void init(Vector<String> args) {
        //Make sure we have >0 arguments
        if (!args.isEmpty())
        {
            //Retrieve the operation to perform
            this.operation = args.get(0);
            
        }
    
    }
}

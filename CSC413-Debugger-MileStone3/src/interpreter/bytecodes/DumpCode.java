package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * DUMP ByteCode, takes 1 argument
 * @author frank
 */
public class DumpCode extends ByteCode {

    String toggle;

    public DumpCode() {
        super("DumpCode");
    }
    /**
     * Tell the VM to toggle the isDumping flag
     * @param vm 
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Toggle debug on or off
        if (this.toggle.equals("ON"))
        {
            vm.dumpOn();
        }
        else
        {
            vm.dumpOff();
        }
    }

    /**
     * Ignore DUMP ByteCode, it should never be printed.
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Do not print DUMP byteCodes
        return null;
    }

    @Override
    public void init(Vector<String> args) {
        //Save dump flag
        if (!args.isEmpty())
        {
            this.toggle = args.get(0);
        }
    }
}

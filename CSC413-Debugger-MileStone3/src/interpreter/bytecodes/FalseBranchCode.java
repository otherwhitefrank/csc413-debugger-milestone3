package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * FALSEBRANCH ByteCode, takes 1 argument
 * @author frank
 */
public class FalseBranchCode extends ByteCode {

    private String jumpAddr; //Address to branch to if false

    public FalseBranchCode() {
        super("FalseBranchCode");
    }
    /**
     * Branches to addr if first value on runStack is false
     * otherwise continues.
     * @param vm 
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Retrieve binary flag to see if we branch
        int branch = vm.popStack();
        if (branch == 0)
        {
            //Branch to addr
            vm.setProgramCounter(Integer.parseInt(this.jumpAddr));
        }
    }

    public String getAddress()
    {
        return this.jumpAddr;
    }
    
    public void setAddress(String addr)
    {
        this.jumpAddr = addr;
    }
    
    /**
     * Standard Debug output
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Print standard sourceByteCode, i.e. "FALSEBRANCH continue<<2>>"
        return this.getSourceByteCode();
    }

    @Override
    public void init(Vector<String> args) {
        if (!args.isEmpty())
        {
            //Parse the jumpAddr
            this.jumpAddr = args.get(0);
        }
    }
}

package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * RETURN ByteCode, maximum of 1 args
 * @author frank
 */
public class ReturnCode extends ByteCode {

    private String comment; //Gratis comment of label we are returning from.
    private int numArgs; //Number of arguments we are processing
    
    public ReturnCode() {
        super("ReturnCode");
    }

    /**
     * Pop the top of the stack Reset the frame Push the value to the top of the
     * stack
     *
     * @param vm
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Pop the current frame
        vm.popFrame();
        
        //Pop the return stack
        int returnAddr = vm.popReturnAddress();
        
        //Set the pc to returnAddr
        vm.setProgramCounter(returnAddr);
    }

    /**
     * Special Debug \Output
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //If no ID present then print standard sourceByteCode
        //If there is then print "RETURN f<<2>>     exit f: 0"
        if (this.numArgs== 0)
        {
            //In case "RETURN" simply print the original sourceByteCode
            return this.getSourceByteCode();
        }
        else
        {
            //Case "RETURN f<<2>>       f(3)"
            String id = this.comment;
            int topOfStack = vm.peekStack();
            String splitID[] = id.split("<");
            String funcID = splitID[0];
            String formattedString = this.getSourceByteCode()
                    + "\texit " + funcID + ": " + topOfStack;
            return formattedString;
        }
    }

    @Override
    public void init(Vector<String> args) {
        if (!args.isEmpty())
        {
            //Store the comment
            this.comment = args.get(0);
            this.numArgs = args.size();
        }        
    }
}

package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.bytecodes.CallCode;
import interpreter.debugger.DebugVirtualMachine;
import java.util.Vector;

/**
 * DebugCallCode extends CallCode and handles source code debugging
 * @author frank
 */
public class DebugCallCode extends CallCode {

    public DebugCallCode() {
        this.byteCodeName = "DebugCallCode";
    }

    @Override
    public void execute(VirtualMachine vm) {
        super.execute(vm);
        try
        {
            //Cast vm upto DebugVM
            DebugVirtualMachine debugVM = (DebugVirtualMachine) vm;

            debugVM.createNewEnvironmentRecord();
            debugVM.setJustEnteredFunction(); //Indicate we just entered a function
                                              //so wait till we hit the Function Bytecode
                                              //to check for a breakpoint
            
        } catch (NumberFormatException e)
        {
            System.out.println("***Error can't cast offset in DebugCall ");
            System.exit(1);
        }
    }

    
}

package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.bytecodes.LitCode;
import interpreter.debugger.DebugVirtualMachine;
import java.util.Vector;

/**
 * DebugLitCode is an extension of LitCode that enter literal into the current environment records
 * symbol table
 * @author frank
 */
public class DebugLitCode extends LitCode {

    private String symbol;
    private int offset;
    private boolean setSymbol = false;

    public DebugLitCode() {
        this.byteCodeName = "DebugLitCode";
    }

    @Override
    public void execute(VirtualMachine vm) {
        super.execute(vm);
        try
        {
            //Cast vm upto DebugVM
            DebugVirtualMachine debugVM = (DebugVirtualMachine) vm;

            if (setSymbol)
            {

                debugVM.enterSymbolAtCurrentOffset(symbol);
            }
        } catch (NumberFormatException e)
        {
            System.out.println("***Error can't cast offset in DebugFormal " + symbol + " " + offset);
            System.exit(1);
        }
    }

    @Override
    public void init(Vector<String> args) {

        super.init(args);
        if (!args.isEmpty())
        {
            if (args.size() == 2)
            {
                setSymbol = true;
                
                //Store the first argument as label
                symbol = args.get(1);
                offset = Integer.valueOf(args.get(0));
            }
        }
    }
}


package interpreter.debugger;

import interpreter.ByteCodeLoader;
import interpreter.CodeTable;
import interpreter.Program;
import interpreter.bytecodes.ByteCode;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

/**
 * DebugByteCodeLoader is an extension of ByteCodeLoader, it handles the new Debug
 * ByteCode's as well as recording valid breakpoints that are needed for the debugger.
 * @author frank
 */
public class DebugByteCodeLoader extends ByteCodeLoader {

    Set<Integer> validBreakpointLines; //Set that contains valid breakpoints
                                       //Each LINE ByteCode is entered in this set
    
    /**
     * Load the ByteCode's in keeping track of which are normal execution and which are for
     * the debugger
     * @return 
     */
    @Override
    public Program loadCodes() {
        try
        {
            do
            {
                //Read the next line
                currString = source.readLine();

                String sourceByteCode = currString;

                //source.readLine() 
                if (currString != null)
                {
                    //Break our line into space seperate tokens
                    String[] splitString = currString.split("\\s");

                    Vector<String> args = new Vector();
                    if (splitString.length <= 0)
                    {
                        ; //Ignore empty lines
                    }
                    else
                    {

                        ByteCode bytecode = null; //Our ByteCode to instantiate into

                        //Check for a DebugByteCode first
                        if (DebugCodeTable.get(splitString[0]) != null)
                        {
                            String codeTok = splitString[0];
                            
                            //If we have a LINE ByteCode then enter the line in our set
                            //that keeps track of valid breakpoints
                            if (codeTok.equals("LINE"))
                            {
                                validBreakpointLines.add(Integer.parseInt(splitString[1]));
                            }
                            
                            //Valid ByteCode token so create the object
                            String codeClass = DebugCodeTable.get(codeTok);
                            
                            bytecode = (ByteCode) (Class.forName("interpreter.bytecodes.debuggerByteCodes." + codeClass).newInstance());
                                                                  
                        }
                        //Check for a regular ByteCode next
                        else if (CodeTable.get(splitString[0]) != null)
                        {
                            String codeTok = splitString[0];
                            //Check regular ByteCode

                            String codeClass = CodeTable.get(codeTok);

                            bytecode = (ByteCode) (Class.forName("interpreter.bytecodes." + codeClass).newInstance());

                        }
                        //Add args to bytecode
                        for (int i = 1; i < splitString.length; i++)
                        {
                            args.add(splitString[i]);
                        }

                        //Init the byteCode with its arguments                            
                        bytecode.init(args);

                        //Add in original sourceByteCode for debug, i.e. "LIT 0 m"
                        bytecode.setSourceByteCode(sourceByteCode);

                        program.addByteCode(bytecode);

                    }

                    //Increment line number
                    lineNum++;
                }
            } while (currString != null);

        } catch (IOException e)
        {
            System.out.println("***Problem in ByteCodeLoader at lineNum: " + lineNum);
            System.exit(1);
        } catch (ClassNotFoundException ex)
        {
            System.out.println("***Problem instantiating class, ClassNotFoundException at lineNum: " + lineNum);
            System.exit(1);
        } catch (InstantiationException ex)
        {
            System.out.println("***Problem instantiating class, InstantiationException at lineNum: " + lineNum);
            System.exit(1);
        } catch (IllegalAccessException ex)
        {
            System.out.println("***Problem instantiating class, IllegalAccessException at lineNum: " + lineNum);
            System.exit(1);
        }

        program.resolveAddress(); //Resolve symbolic addresses 

        return program;
    }

    /**
     * Default constructor, open the programFile and waits for a call to loadCodes()
     * @param programFile
     * @throws IOException 
     */
    public DebugByteCodeLoader(String programFile) throws IOException {
        super(programFile);
        
        validBreakpointLines = new HashSet<>();
        
    }
    
    
    /**
     * Called after loadCodes(), this returns a Set of valid breakpoints for the debugger.
     * @return 
     */
    public Set<Integer> getValidBreakpointLines()
    {
        return validBreakpointLines;
    }

}

package interpreter.debugger;

import interpreter.Program;
import interpreter.VirtualMachine;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;

/**
 * DebugVirtualMachine is a child of VirtualMachine. It is run by the
 * UserInterface to allow for higher level processing of uncompiled .x source
 * code
 *
 * @author frank
 */
public class DebugVirtualMachine extends VirtualMachine {

    //Vector of our sourcelines and breakpoints
    private Vector<Entry> sourceCodeLines; //A vector of Entry's, one for each line of sourceCode
    private Stack<FunctionEnvironmentRecord> environmentStack; //A stack to hold FunctionEnvironmentRecord

    private Set<Integer> validBreakpointLines; //A set that contains the lines that are valid to break on
    private FunctionEnvironmentRecord currRecord; //Reference to the current record.

    private BufferedReader source; //BufferedReader for our source file

    private boolean debugJustEnteredFunction = false;
    private boolean programTerminated = false;

    /**
     * Default constructor, takes a Program object from the DebugByteCodeLoader
     * A String that points to the sourceCodeFileName which will be opened and
     * processed and a Set containing all the valid lines to set breakpoints at.
     *
     * @param program
     * @param sourceCodeFileName
     * @param validBreakPoints
     */
    public DebugVirtualMachine(Program program, String sourceCodeFileName, Set<Integer> validBreakPoints) {
        super(program); //Call base VirtualMachine
        try
        {
            //Initialize sourceCodeLines and environmentStack
            sourceCodeLines = new Vector<Entry>();
            environmentStack = new Stack<FunctionEnvironmentRecord>();

            //Setup validBreakPoints
            this.validBreakpointLines = validBreakPoints;

            //Open the sourceFile
            source = new BufferedReader(new FileReader(sourceCodeFileName));

            String currString;

            do
            {
                //Read the next line
                currString = source.readLine();

                if (currString != null)
                {
                    Entry currEntry = new Entry(currString, false);

                    sourceCodeLines.add(currEntry); //Add line to sourceCodeLines
                }

            } while (currString != null);

            //Create an initial FunctionEnvironmentRecord
            FunctionEnvironmentRecord firstRecord = new FunctionEnvironmentRecord();
            firstRecord.setStartLine(1);
            firstRecord.setEndLine(sourceCodeLines.size());
            environmentStack.push(firstRecord);

        } catch (FileNotFoundException ex)
        {
            System.out.println("***IOException cannot find file: " + sourceCodeFileName);
            System.exit(1);
        } catch (IOException ex)
        {
            System.out.println("***IOException can't read from file: " + sourceCodeFileName);
            System.exit(1);
        }

    }

    /**
     * UserInterface calls this when a user continues execution. This then calls
     * the underlying VirtualMachine. This is done to prevent the user from
     * continuing once a program has halted.
     */
    public boolean debugExecuteProgram() {
        if (!programTerminated)
        {
            //Make sure the program has not already terminated
            //I.E. its ran through and hit HALT already
            this.executeProgram();
            return true;
        }
        else
        {
            System.out.println("Program has already finished execution!");
            return false;
        }
    }

    /**
     * Create a new environment record and push it onto the environmentStack
     */
    public void createNewEnvironmentRecord() {
        //Create a new EnvionrmentRecord now
        FunctionEnvironmentRecord currEnv = new FunctionEnvironmentRecord();
        environmentStack.push(currEnv);
    }

    /**
     * Used keep track of if we have just entered a function
     */
    public void setJustEnteredFunction() {
        this.debugJustEnteredFunction = true;
    }

    /**
     * UserInterface calls this when execution is halted, this is to prevent a
     * user from continuing execution after a program has halted.
     */
    @Override
    public void haltExecution() {

        super.haltExecution();

        //Set programTerminated to true and then call super function
        programTerminated = true;

        //Set current execution line to the last one in the functionEnvironmentRecord
        FunctionEnvironmentRecord currRecord = this.getCurrentEnvironmentRecord();

        //Makes -> goto end of source code for prettier printing
        currRecord.setCurrentLine(currRecord.getStopLine());

        //System.out.println("******Execution Halted*******");
    }

    /**
     * Remove justEnteredFunction flag
     */
    public void unsetJustEnteredFunction() {
        this.debugJustEnteredFunction = false;
    }

    /**
     * Set a functions name, startline and endline in the source code.
     *
     * @param funcName
     * @param startLine
     * @param stopLine
     */
    public void setFunction(String funcName, int startLine, int stopLine) {
        if (!environmentStack.empty())
        {
            environmentStack.peek().setFunction(funcName, startLine, stopLine);
        }

        unsetJustEnteredFunction(); //Unset the flag since we have processed the current function.

        //Check to see if new line has a breakpoint at it.
        if (!(startLine < 1))
        {
            if (sourceCodeLines.get(startLine - 1).isBreakptSet)
            {
                //Tell underlying VM to halt
                super.haltExecution();
            }
        }
    }

    /**
     * Retrieve the current environment record
     *
     * @return
     */
    public FunctionEnvironmentRecord getCurrentEnvironmentRecord() {
        return this.environmentStack.peek();
    }

    /**
     * Set the current execution line in the current environment record
     *
     * @param currentLine
     */
    public void setCurrentLine(int currentLine) {
        if (!environmentStack.empty())
        {
            environmentStack.peek().setCurrentLine(currentLine);
        }

        if (!debugJustEnteredFunction)
        {
            //Check to see if new line has a breakpoint at it.
            if (!(currentLine < 1))
            {
                if (sourceCodeLines.get(currentLine - 1).isBreakptSet)
                {
                    //Tell underlying VM to halt
                    super.haltExecution();
                }
            }
        }

    }

    /**
     * pop n symbols from environment stack
     *
     * @param n
     */
    public void popSymbolTable(int n) {
        //Pop the symbols for environmentStack
        if (!environmentStack.empty())
        {
            environmentStack.peek().pop(n);
        }
    }

    /**
     * Pop the top environment record
     */
    public void popEnvironmentRecord() {
        //Pop the top environmentRecord
        if (!environmentStack.empty())
        {
            environmentStack.pop();
        }
    }

    /**
     * Retrieve the current environment records source code
     *
     * @return
     */
    public Vector<Entry> getCurrentFunctionSource() {
        //Return the source code for the currently executing function
        //If there is no entry on environmentStack then we return the entire program

        Vector<Entry> currFunctionEntries;

        if (environmentStack.empty())
        {
            //No record started so return the entire program
            currFunctionEntries = sourceCodeLines;
        }
        else
        {
            //Stack is not empty so retrieve the top of environmentStack
            FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

            int startLine = 0;
            int stopLine = 0;

            //Get the start and stop lines.
            startLine = currEnvRecord.getStartLine();
            stopLine = currEnvRecord.getStopLine();

            //Create a new vector
            currFunctionEntries = new Vector<Entry>();

            for (int i = (startLine - 1); i < stopLine; i++)
            {
                currFunctionEntries.add(sourceCodeLines.get(i));
            }

        }

        //Return the reference to our sourceCode
        return currFunctionEntries;

    }

    /**
     * <pre>Get the current execution line from the top of the environmentStack</pre>
     *
     * @return
     */
    public int getCurrentExecutionLine() {
        if (environmentStack.empty())
        {
            return 0;
        }
        else
        {
            FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

            return currEnvRecord.getCurrentLine();
        }
    }

    /**
     * <pre>Get the current function start line from the top of the environmentStack</pre>
     *
     * @return
     */
    public int getCurrentFunctionStartLine() {

        FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

        return currEnvRecord.getStartLine();

    }

    /**
     * <pre>Get the current function stop line from the top of the environmentStack</pre>
     *
     * @return
     */
    public int getCurrentFunctionStopLine() {

        FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

        return currEnvRecord.getStopLine();

    }

    /**
     * Enter a symbol at the current offset in the runStack
     *
     * @param sym
     */
    public void enterSymbolAtCurrentOffset(String sym) {
        if (!environmentStack.empty())
        {
            //Query runStack for current offset
            int runStackPtr = this.runStack.getCurrentStackPtr();

            //Adjust for starting at index 0
            runStackPtr--;

            FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

            currEnvRecord.enterSym(sym, runStackPtr);
        }
    }

    /**
     * Enter a symbol at the specified offset in the environment record
     *
     * @param sym
     * @param offset
     */
    public void enterSymbolAtOffset(String sym, int offset) {
        if (!environmentStack.empty())
        {

            FunctionEnvironmentRecord currEnvRecord = environmentStack.peek();

            currEnvRecord.enterSym(sym, offset);
        }
    }

    /**
     * Set a breakpoint at lineNum
     *
     * @param lineNum
     * @return
     */
    public boolean setBreakPoint(int lineNum) {
        if (validBreakpointLines.contains(lineNum))
        {
            //lineNum is valid to set BreakPoint
            sourceCodeLines.get(lineNum - 1).setBreak();
            return true;
        }
        else
        {
            return false; //Not valid line to break on, so send back false.
        }
    }

    /**
     * Unset a breakpoint at lineNum
     *
     * @param lineNum
     * @return
     */
    public boolean unsetBreakPoint(int lineNum) {
        if (validBreakpointLines.contains(lineNum))
        {
            //lineNum is valid to set BreakPoint
            sourceCodeLines.get(lineNum - 1).unsetBreak();
            return true;
        }
        else
        {
            return false; //Not valid line to break on, so send back false.
        }
    }

    /**
     * Unset all breakpoints
     */
    public void clearAllBreakpoints() {
        //Iterate over all breakpoints
        Iterator<Integer> breaks = validBreakpointLines.iterator();

        while (breaks.hasNext())
        {
            int i = breaks.next();
            //Make sure i is positive so we dont get Read/Write intrinsic functions
            if (i > 0)
            {
                //i is valid to unset BreakPoint
                sourceCodeLines.get(i - 1).unsetBreak();
            }
        }
    }
}

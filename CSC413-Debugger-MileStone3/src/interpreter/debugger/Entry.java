
package interpreter.debugger;

/**
 * <pre>Each Entry contains a single line of source and a boolean to indicate if its breakpoint
 * is set on that line. </pre>
 * @author frank
 */

public class Entry {

    String sourceLine; //A line of sourceCode

    
    boolean isBreakptSet; //Break at this line?

    /**
     * Return the line of source code
     * @return sourceLine
     */
    public String getSource()
    {
        return this.sourceLine;
    }
    
    /**
     * Return if a breakpoint is set in this Entry
     * @return isBreakptSet
     */
    public boolean getBreakSet()
    {
        return this.isBreakptSet;
    }
    
    /**
     * Set a breakpoint at this entry
     */
    public void setBreak()
    {
        isBreakptSet = true;
    }
    
    /**
     * Unset a breakpoint at this entry
     */
    public void unsetBreak()
    {
        isBreakptSet = false;
    }
    
    /**
     * Manually set the line of source code
     * @param src 
     */
    public void setSource(String src)
    {
        sourceLine = src;
    }
    
    /**
     * Default constructor, takes a line of sourceCode and a whether there is a breakpt set
     * @param inLine
     * @param breakSet 
     */
    public Entry(String inLine, boolean breakSet) {
        //Set default values
        this.sourceLine = inLine;
        this.isBreakptSet = breakSet;
    }
}
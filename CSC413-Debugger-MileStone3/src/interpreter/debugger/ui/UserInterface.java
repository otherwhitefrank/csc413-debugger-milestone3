package interpreter.debugger.ui;

import interpreter.Program;
import interpreter.debugger.DebugVirtualMachine;
import interpreter.debugger.Entry;
import interpreter.debugger.FunctionEnvironmentRecord;
import interpreter.debugger.SymbolTable;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;

/**
 * Provides user interface for the .X language debugger
 *
 * @author frank
 */
public class UserInterface {

    DebugVirtualMachine debugVM; //Reference to the debugVM

    /**
     * Create a user interface with a valid Program, sourceCodeFileName, and a
     * Set of the validBreakPoints
     *
     * @param program An initialized program file from the DebugByteCodeLoader
     * @param sourceCodeFileName A string pointing to the source code file
     * @param validBreakPoints A set containing all the valid breakpoints, LINE
     * Bytecodes
     */
    public UserInterface(Program program, String sourceCodeFileName, Set<Integer> validBreakPoints) {
        //Create DebugVM, this reads the sourceCode from the file and then opens the old interpreter as usual.
        debugVM = new DebugVirtualMachine(program, sourceCodeFileName, validBreakPoints);
    }

    /**
     * Begin executing the debugger user interface
     */
    public void run() {
        String input = ""; //Holder for user input
        Scanner keyboard = new Scanner(System.in);

        //Display source
        displayFunctionSource();
        System.out.println("Type ? for help");

        do
        {

            this.displayPrompt();
            //Process input
            input = keyboard.nextLine();

            String seperatedInput[] = input.split("\\s");
            if (!(seperatedInput[0].equals("q")))
            {
                processInput(seperatedInput);
            }

            //Tell debugVM to begin execution, once it finishes it returns to this loop.
        } while (!input.equals("q"));

    }

    /**
     * Display the user prompt
     */
    private void displayPrompt() {
        System.out.print("> ");
    }

    /**
     * Display the current executing functions source code with breakpoint and
     * execution indicators
     */
    private void displayFunctionSource() {
        //Ask the debugVM for a vector of Entry's to print the source code
        Vector<Entry> currentFunctionSource = debugVM.getCurrentFunctionSource();
        int currentLineIndex = debugVM.getCurrentExecutionLine();

        int startLine = debugVM.getCurrentFunctionStartLine();
        int stopLine = debugVM.getCurrentFunctionStopLine();

        currentLineIndex = currentLineIndex - startLine; //Adjust for starting at index 0

        for (int i = 0; i < currentFunctionSource.size(); i++)
        {
            String outputString = "";
            Entry currEntry = currentFunctionSource.get(i);

            //Print breakPoint if its set
            if (currEntry.getBreakSet())
            {
                outputString += " *";
            }
            else
            {
                outputString += "  ";
            }

            //Print line number
            outputString += String.format(" %3d.", (i + startLine));

            outputString += " " + String.format("%-40s", currEntry.getSource());

            //Print --> at current line of execution
            if (i == currentLineIndex)
            {
                outputString += String.format("%-3s", "  <----");

            }
            else
            {
                outputString += String.format("%-3s", "       ");
            }

            System.out.println(outputString);
        }

    }

    /**
     * Process a single line of input
     *
     * @param input a tokenized line of input
     */
    private void processInput(String[] input) {
        String command = "";
        command = input[0];

        //Quit
        if (command.equalsIgnoreCase("q"))
        {
            ;//Quit command, ignore and the loop will exit after
        }
        //Step Over
        else if ((command.equalsIgnoreCase("so")))
        {
            System.out.println("Step Over");
        }
        //Step Into
        else if ((command.equalsIgnoreCase("si")))
        {
            System.out.println("Step Into");
        }
        //Step Out
        else if ((command.equalsIgnoreCase("sout")))
        {
            System.out.println("Step Out");
        }
        //Set Breakpoint
        else if ((command.equalsIgnoreCase("set")))
        {
            try
            {
                for (int i = 1; i < input.length; i++)
                {
                    int breakLine;
                    breakLine = Integer.valueOf(input[i]);
                    if (debugVM.setBreakPoint(breakLine))
                    {
                        //Correctly set breakpoint
                        System.out.println("Breakpoint Set at line: " + breakLine);
                    }
                    else
                    {
                        //Incorrect line, can't set a break here.
                        System.out.println("***Error: Can't set breakpoint at line: " + breakLine);
                    }
                }

            } catch (NumberFormatException e)
            {
                System.out.println("***Error: set only takes integer values ");
            } catch (ArrayIndexOutOfBoundsException e)
            {
                System.out.println("***Error: Invalid breakpoint");
            }
        }

        //UnSet Breakpoint
        else if ((command.equalsIgnoreCase("clr")))
        {
            try
            {

                //If length == 0 then we received "clr" so clear all breakPoints
                if (input.length == 1)
                {
                    debugVM.clearAllBreakpoints();
                    System.out.println("All breakpoints cleared");
                }
                //If length is > 0 then we received a list of comma seperated values
                //I.E. "clr 12,13,14"
                else if (input.length > 1)
                {

                    for (int i =1; i < input.length; i++)
                    {
                        int breakLine;
                        breakLine = Integer.valueOf(input[i]);
                        if (debugVM.unsetBreakPoint(breakLine))
                        {
                            //Correctly set breakpoint
                            System.out.println("Breakpoint removed at line: " + breakLine);
                        }
                        else
                        {
                            //Incorrect line, can't set a break here.
                            System.out.println("***Error: Can't remove breakpoint at line: " + breakLine);
                        }
                    }
                }
            } catch (NumberFormatException e)
            {
                System.out.println("***Error: Step only takes integer values ");
            }
        }

//        //List Breakpoints
//        else if ((command.equalsIgnoreCase("lb")))
//        {
//            System.out.println("List Breakpoints");
//        }
//        //Clear Breakpoints
//        else if ((command.equalsIgnoreCase("cb")))
//        {
//            System.out.println("Clear Breakpoints");
//        }
//        //Function Tracing
//        else if ((command.equalsIgnoreCase("trace")))
//        {
//            System.out.println("Function Trace");
//        }
//        //Print Call Stack
//        else if ((command.equalsIgnoreCase("pcs")))
//        {
//            System.out.println("Print Call Stack");
//        }
        //Print Variables
        else if ((command.equalsIgnoreCase("pvar")))
        {
            printLocalVariables();
        }
        //Print Source Code
        else if ((command.equalsIgnoreCase("ps")))
        {
            this.displayFunctionSource();
        }

        //Continue Execution
        else if ((command.equalsIgnoreCase("ce")))
        {

            if (debugVM.debugExecuteProgram()) //Special execute that makes sure debugger hasn't already terminated
            {
                //Program has not terminated, okay to execute()
                //Display prompt
                this.displayFunctionSource();
            }
        }
        //Halt Expression
        else if ((command.equalsIgnoreCase("halt")))
        {
            System.out.println("Halt");
        }
        //Print Help
        else if ((command.equalsIgnoreCase("?")) || (command.equalsIgnoreCase("help")))
        {
            this.printHelp();
        }
        //Unknown Command
        else
        {
            System.out.println("Unknown Command!");
        }
    }

    /**
     * Print the user help screen
     */
    private void printHelp() {
        System.out.println("**************************HELP****************************");
        System.out.println("* '?' or 'help'    - Print Help Screen                   *");
        System.out.println("* 'q'              - Quit Debugger                       *");
        //System.out.println("* 'so'             - Step Over                         *");
        //System.out.println("* 'si'             - Step Into                         *");
        //System.out.println("* 'sout'           - Step Out                          *");
        System.out.println("* 'set <line #>'   - Set Breakpoint at <line #>          *");
        System.out.println("*  --set takes one or more <line #> seperated by spaces  *");
        System.out.println("* 'clr'            - Clear All Breakpoints               *");
        System.out.println("* 'clr <line #>'   - Clear Breakpoint at <line #>        *");
        System.out.println("*  --clr takes zero or more <line #> seperated by spaces *");

        //System.out.println("* 'lb'             - List Breakpoints                  *");
        //System.out.println("* 'cb'             - Clear BreakPoints                 *");
        //System.out.println("* 'trace'          - Toggle Function Trace <on/off>    *");
        //System.out.println("* 'pcs'            - Print Call Stack                  *");
        System.out.println("* 'pvar'           - Print Local Variables               *");
        System.out.println("* 'ps'             - Print Source Code                   *");
        System.out.println("* 'ce'             - Continue Execution                  *");
        //System.out.println("* 'halt'           - Halt Debugger                     *");
        System.out.println("**********************************************************");

    }

    /**
     * Print the local variables within the current functions execution
     */
    public void printLocalVariables() {
        //Get currentRecord
        FunctionEnvironmentRecord currRecord;
        currRecord = debugVM.getCurrentEnvironmentRecord();

        SymbolTable symTable = currRecord.getSymbolTable();

        Set<String> keys = symTable.keys();

        //System.out.println("***********Variables**************");
        if (keys.isEmpty())
        {
            System.out.println("No Local Variables");
        }

        for (String e : keys)
        {
            Object obj = symTable.get(e);
            int offset = (int) obj;

            int value = debugVM.getValAtOffset(offset);

            System.out.println(e + " : " + value);
        }

    }

}
